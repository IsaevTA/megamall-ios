//
//  NetworkService.swift
//  MegaMall
//
//  Created by Timur Isaev on 04.12.2019.
//  Copyright © 2019 Timur Isaev. All rights reserved.
//

import Foundation
import Alamofire

class NetworkService {
    
    //Создание HTTP POST запроса
    static func createRequest(urlString: String, parameters: [String: String?]) -> URLRequest {
    
        print("URL: " + urlString)
        print(parameters)
        
        let url = URL(string: urlString)
    
        let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: [])
        
        var request = URLRequest(url: url!)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.httpBody = httpBody
        
        return request
    }
    
    static func getUser(vc: UIViewController, goToMainViewController: Bool) {
        
        let phoneCurrentUser = UserDefaults.standard.string(forKey: "MEGAMALLPHONE")
        let uidCurrentUser = UserDefaults.standard.string(forKey: "MEGAMALLUID")
        
        let parameters = ["phone": phoneCurrentUser, "uid": uidCurrentUser]
        
        vc.showActivityIndicator(textLabel: "Вход пользователя")
        
        let request = Network.createRequest(urlString: Network.urlLogin, parameters: parameters).self
        let session = URLSession.shared
        
        
        session.dataTask(with: request) { (data, response, error) in
            vc.hideActivityIndicator()
            
            guard let data = data else { return }
            do {
                let jsonResult = try JSONSerialization.jsonObject(with: data, options:JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                if jsonResult.value(forKey: "success") as! Bool == false {
                    print("Root - success - false")
                    vc.showAlertView(sendingVC: vc, titleString: "ERROR", messageString: "Пользователь не зарегистрирован. Для продолжения работы необходимо зарегистрироватся.")
                } else {
                    print("Login - success - true")
                    let jsonAuth = try JSONDecoder().decode(User.self, from: data)
                    //print(jsonAuth)
                    currentUser = jsonAuth.user
                    UserDefaults.standard.set(jsonAuth.user.api_token, forKey: "CURRENTTOKENUSER")
                    if goToMainViewController {
                        vc.goMainViewController()
                    }
                }
                
                
            } catch {
                vc.showAlertView(sendingVC: vc, titleString: "ERROR", messageString: error as! String)
            }
        }.resume()
    }
    
    static func checkUser(phoneUser: String, completion: @escaping (_ checkUserPhone: Bool)->()) {
        
        let parameters = ["phone": phoneUser]
        let request = Network.createRequest(urlString: Network.urlCheckPhone, parameters: parameters).self
        let session = URLSession.shared

        session.dataTask(with: request) { (data, response, error) in
            
            guard let data = data else { return }
            
            do {
                
                let jsonResult = try JSONSerialization.jsonObject(with: data, options:JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                let checkUserPhone: Bool = (jsonResult.value(forKey: "success") != nil)
                
                completion(checkUserPhone)
            } catch {
                print(error)
                
            }
        }.resume()
    }
    
    static func uploadAvatar(userID: Int, avatarUser: UIImage) {
        
        guard let id = currentUser?.id else { return }
        
        let parameters = ["photo": avatarUser.pngData()]
        
        let urlString: String! = Network.urlUploadAvatar + String(describing: id)
        
        let url = URL(string: urlString)
    
        let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: [])
        
        var request = URLRequest(url: url!)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.httpBody = httpBody
        
        let session = URLSession.shared

        session.dataTask(with: request) { (data, response, error) in
            
            guard let data = data else { return }

            do {
                
                let jsonResult = try JSONSerialization.jsonObject(with: data, options:JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                print(jsonResult)
            } catch {
                print(error)
            }
        }.resume()
    }
}
