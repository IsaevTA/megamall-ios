//
//  Network.swift
//  MegaMall
//
//  Created by Timur Isaev on 24.11.2019.
//  Copyright © 2019 Timur Isaev. All rights reserved.
//

import Foundation
import UIKit

class Network {

    //Адрес API
    static let url = "https://megamall.yorichdevelop.ru"

    //URL для проверки на регистрацию пользователя
    static let urlCheckPhone = Network.url + "/api/user/fb/isset"
    //URL для регистриции
    static let urlSingUp = Network.url + "/api/user/fb/save"
    //URL авторизация
    static let urlLogin = Network.url + "/api/user/fb/login"
    //URL редактирование пользователя
    static let urlEditUser = Network.url + "/api/user/"
    //URL отправки аватарки
    static let urlUploadAvatar = Network.url + "/api/user/avatar/"
    
    //Создание HTTP POST запроса
    static func createRequest(urlString: String, parameters: [String: String?]) -> URLRequest {
    
        print("URL: " + urlString)
        print(parameters)
        
        let url = URL(string: urlString)
    
        let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: [])
        
        var request = URLRequest(url: url!)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.httpBody = httpBody
        
        return request
    }
}
