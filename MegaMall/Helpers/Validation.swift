//
//  Validation.swift
//  MegaMall
//
//  Created by Timur Isaev on 24.11.2019.
//  Copyright © 2019 Timur Isaev. All rights reserved.
//

import UIKit

class Validation {
    
    static func validationPhoneNumber(phoneNumber: String) -> Bool {
       let phoneRegex = "^[0-9+]{0,1}+[0-9]{5,16}$"
       let phoneTest = NSPredicate(format: "SELF MATCHES %@", phoneRegex)
       return phoneTest.evaluate(with: phoneNumber)
    }
    
}
