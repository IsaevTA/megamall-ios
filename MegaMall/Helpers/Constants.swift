//
//  Constants.swift
//  CustomLoginDemo
//
//  Created by Christopher Ching on 2019-07-23.
//  Copyright © 2019 Christopher Ching. All rights reserved.
//

import Foundation

struct Constants {
    
    struct Storyboard {

        static let rootViewController = "RootViewController"
        static let loginViewController = "LoginViewController"
        static let singUpViewController = "SingUpViewController"
        static let getCodeViewController = "GetCodeViewController"
        static let tabBarController = ""
        static let navigationRootViewController = "NavigationRootViewController"
        static let tabBarViewController = "TabBarViewController"
    }
}
