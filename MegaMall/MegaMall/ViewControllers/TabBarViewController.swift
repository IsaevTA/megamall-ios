//
//  TabBarViewController.swift
//  MegaMall
//
//  Created by Timur Isaev on 27.11.2019.
//  Copyright © 2019 Timur Isaev. All rights reserved.
//

import UIKit

class TabBarViewController: UITabBarController {

    static func storyboardInstance() -> TabBarViewController? {
        let storyboard = UIStoryboard(name: "TabBarViewController", bundle: nil)
        return storyboard.instantiateInitialViewController() as? TabBarViewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}
