//
//  RegistryViewController.swift
//  MegaMall
//
//  Created by Timur Isaev on 22.11.2019.
//  Copyright © 2019 Timur Isaev. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth

class GetCodeViewController: UIViewController {
    
    @IBOutlet weak var fistNumberCodeTextField: UITextField!
    @IBOutlet weak var secondNumberCodeTextField: UITextField!
    @IBOutlet weak var thirdNumberCodeTextField: UITextField!
    @IBOutlet weak var fourthNumberCodeTextField: UITextField!
    @IBOutlet weak var fifthNumberCodeTextField: UITextField!
    @IBOutlet weak var sixthNumberCodeTextField: UITextField!
    @IBOutlet weak var actionButton: UIButton!
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    // Screen width.
    var screenWidth: CGFloat { return UIScreen.main.bounds.width }
    // Screen height.
    var screenHeight: CGFloat { return UIScreen.main.bounds.height }
    
    var singUp: Bool = false
    
    deinit {
        removeKeyboardNotifications()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        UISettings()
        registerForKeyboardNotifications()
    }

    func UISettings() {
        
        navigationController?.isNavigationBarHidden = false
        
        fistNumberCodeTextField.text = ""
        secondNumberCodeTextField.text = ""
        thirdNumberCodeTextField.text = ""
        fourthNumberCodeTextField.text = ""
        fifthNumberCodeTextField.text = ""
        sixthNumberCodeTextField.text = ""
        fistNumberCodeTextField.becomeFirstResponder()
        
        actionButton.isEnabled = false
        actionButton.backgroundColor = UIColor.lightGray
    }
    
    func registerForKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(kbWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(kbWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    func removeKeyboardNotifications() {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func kbWillShow(_ notification: Notification) {
        topConstraint.constant = 30
    }
    
    @objc func kbWillHide() {
        if screenHeight == CGFloat(896) {
            topConstraint.constant = 310
        } else if screenHeight == CGFloat(812) {
            topConstraint.constant = 308
        } else if screenHeight == CGFloat(736) {
            topConstraint.constant = 310
        } else if screenHeight == CGFloat(667) {
            topConstraint.constant = 240
        }else if screenHeight == CGFloat(568) {
            topConstraint.constant = 140
        } else if screenHeight == CGFloat(480) {
            topConstraint.constant = 55
        } else {
            topConstraint.constant = 50
        }
    }
    
    @IBAction func singUpButton(_ sender: UIButton) {
                
        let verificationID = UserDefaults.standard.string(forKey: "authVerificationID")
        let verificationCode = getVerificationCode()
        
        if verificationCode == "" {
            self.showAlertView(sendingVC: self, titleString: "ERROR", messageString: "Введите код проверки")
        } else {
            self.showActivityIndicator(textLabel: "Проверка кода")

            let credential = PhoneAuthProvider.provider().credential(withVerificationID: verificationID!, verificationCode: verificationCode)
            Auth.auth().signIn(with: credential) { (authResult, error) in
                self.hideActivityIndicator()
                
                if error != nil {
                    self.showAlertView(sendingVC: self, titleString: "ERROR", messageString: error?.localizedDescription ?? "")
                    return
                } else {
                    let phone = authResult?.user.phoneNumber ?? ""
                    let uid = authResult?.user.uid ?? ""
                    let parameters = ["phone": phone, "uid": uid]
 
                    UserDefaults.standard.set(phone, forKey: "MEGAMALLPHONE")
                    UserDefaults.standard.set(uid, forKey: "MEGAMALLUID")
                    
                    print(self.singUp ? Network.urlSingUp : Network.urlLogin)
                    print(parameters)
                    
                    self.showActivityIndicator(textLabel: self.singUp ? "Регистрация пользователя" : "Вход пользователя")
    
                    let request = Network.createRequest(urlString: self.singUp ? Network.urlSingUp : Network.urlLogin, parameters: parameters).self
                    let session = URLSession.shared
                    
                    session.dataTask(with: request) { (data, response, error) in
                        self.hideActivityIndicator()
                        
                        guard let data = data else { return }

                        do {
//                            let json1 = try JSONSerialization.jsonObject(with: data, options: [])
//                            print(json1)
                            let jsonResult = try JSONSerialization.jsonObject(with: data, options:JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                            if jsonResult.value(forKey: "success") as! Bool == false {
                                if self.singUp {
                                    self.showAlertView(sendingVC: self, titleString: "ERROR", messageString: "Пользователь уже зарегистрирован. Для продолжения работы пройдите авторизацию.")
                                } else {
                                    self.showAlertView(sendingVC: self, titleString: "ERROR", messageString: "Пользователь не зарегистрирован. Для продолжения работы необходимо зарегистрироватся.")
                                }
                            } else {
                                let jsonAuth = try JSONDecoder().decode(User.self, from: data)
                                print(jsonAuth)
                                currentUser = jsonAuth.user
                                UserDefaults.standard.set(jsonAuth.user.api_token, forKey: "CURRENTTOKENUSER")
                                self.goMainViewController()
                            }
                        } catch {
                            self.showAlertView(sendingVC: self, titleString: "ERROR", messageString: error as! String)
                        }
                    }.resume()
                }
            }
        }

    }
    
    func getVerificationCode() -> String {
        
        let tempCode1 = fistNumberCodeTextField.text!.trimmingCharacters(in: .whitespaces)
        let tempCode2 = secondNumberCodeTextField.text!.trimmingCharacters(in: .whitespaces)
        let tempCode3 = thirdNumberCodeTextField.text!.trimmingCharacters(in: .whitespaces)
        let tempCode4 = fourthNumberCodeTextField.text!.trimmingCharacters(in: .whitespaces)
        let tempCode5 = fifthNumberCodeTextField.text!.trimmingCharacters(in: .whitespaces)
        let tempCode6 = sixthNumberCodeTextField.text!.trimmingCharacters(in: .whitespaces)
        
        if tempCode1 == "" { return "" }
        if tempCode2 == "" { return "" }
        if tempCode3 == "" { return "" }
        if tempCode4 == "" { return "" }
        if tempCode5 == "" { return "" }
        if tempCode6 == "" { return "" }
        
        return tempCode1 + tempCode2 + tempCode3 + tempCode4 + tempCode5 + tempCode6
    }
}


extension GetCodeViewController: UITextFieldDelegate {

    private func getCountTextFields() -> Int {
        return fistNumberCodeTextField.text!.trimmingCharacters(in: .whitespaces).count + secondNumberCodeTextField.text!.trimmingCharacters(in: .whitespaces).count + thirdNumberCodeTextField.text!.trimmingCharacters(in: .whitespaces).count + fourthNumberCodeTextField.text!.trimmingCharacters(in: .whitespaces).count + fifthNumberCodeTextField.text!.trimmingCharacters(in: .whitespaces).count + sixthNumberCodeTextField.text!.trimmingCharacters(in: .whitespaces).count

        //return tempCode1.count + tempCode2.count + tempCode3.count + tempCode4.count + tempCode5.count + tempCode6.count
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

        if getCountTextFields() == 5 && string != "" {
            actionButton.isEnabled = true
            actionButton.backgroundColor = UIColor(red: 0.01, green: 0.30, blue: 0.86, alpha: 1)
        } else {
            actionButton.isEnabled = false
            actionButton.backgroundColor = UIColor.lightGray
        }

        
        if textField == sixthNumberCodeTextField {

            if fifthNumberCodeTextField.text!.count < 1 {
                fifthNumberCodeTextField.becomeFirstResponder()
                return false
            }
            if string != "" && sixthNumberCodeTextField.text!.count < 1 {
                sixthNumberCodeTextField.insertText(string)
                return false
            }
        }
        if textField == fifthNumberCodeTextField {
            return checkBackTextField(tempString: string, fistTextField: fourthNumberCodeTextField, secondTextField: fifthNumberCodeTextField, thirTextField: sixthNumberCodeTextField)
        }
        if textField == fourthNumberCodeTextField {
            return checkBackTextField(tempString: string, fistTextField: thirdNumberCodeTextField, secondTextField: fourthNumberCodeTextField, thirTextField: fifthNumberCodeTextField)
        }
        if textField == thirdNumberCodeTextField {
            return checkBackTextField(tempString: string, fistTextField: secondNumberCodeTextField, secondTextField: thirdNumberCodeTextField, thirTextField: fourthNumberCodeTextField)
        }
        if textField == secondNumberCodeTextField {
            
            return checkBackTextField(tempString: string, fistTextField: fistNumberCodeTextField, secondTextField: secondNumberCodeTextField, thirTextField: thirdNumberCodeTextField)
        }
        if textField == fistNumberCodeTextField {
            return chekAndInsertString(tempString: string, fistTextField: fistNumberCodeTextField, secondTextField: secondNumberCodeTextField)
        }
        
        return true
    }
    
    func chekAndInsertString(tempString: String, fistTextField: UITextField, secondTextField: UITextField) -> Bool {
        
        if tempString != "" && fistTextField.text!.count < 1 {
            fistTextField.insertText(tempString)
            secondTextField.text = ""
            secondTextField.becomeFirstResponder()
            return false
        }
        return true
    }
    
    func checkBackTextField(tempString: String, fistTextField: UITextField, secondTextField: UITextField, thirTextField: UITextField) -> Bool {
        
        if fistTextField.text!.count < 1 {
            fistTextField.becomeFirstResponder()
            return false
        }

        if tempString != "" && secondTextField.text!.count < 1 {
            secondTextField.insertText(tempString)
            thirTextField.text = ""
            thirTextField.becomeFirstResponder()
            return false
        }
        
        return true
    }
}
