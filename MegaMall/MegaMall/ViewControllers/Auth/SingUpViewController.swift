//
//  SingUpViewController.swift
//  MegaMall
//
//  Created by Timur Isaev on 24.11.2019.
//  Copyright © 2019 Timur Isaev. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth

class SingUpViewController: UIViewController {

    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var mailTextField: UITextField!
    @IBOutlet weak var phoneMailSegment: UISegmentedControl!
    @IBOutlet weak var actionButton: UIButton!
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    // Screen width.
    var screenWidth: CGFloat { return UIScreen.main.bounds.width }
    // Screen height.
    var screenHeight: CGFloat { return UIScreen.main.bounds.height }
    
    deinit {
        removeKeyboardNotifications()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        UISettings()
        registerForKeyboardNotifications()
    }

    func UISettings() {
        
        navigationController?.isNavigationBarHidden = false
        
        phoneTextField.text = "+7 (9"
        phoneTextField.becomeFirstResponder()
        
        phoneMailSegment.selectedSegmentIndex = 0
        
        phoneTextField.isHidden = false
        mailTextField.isHidden = true
        
        actionButton.isEnabled = false
        actionButton.backgroundColor = UIColor.lightGray
    }
    
    func registerForKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(kbWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(kbWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    func removeKeyboardNotifications() {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func kbWillShow(_ notification: Notification) {
        topConstraint.constant = 30
    }
    
    @objc func kbWillHide() {
        if screenHeight == CGFloat(896) {
            topConstraint.constant = 310
        } else if screenHeight == CGFloat(812) {
            topConstraint.constant = 308
        } else if screenHeight == CGFloat(736) {
            topConstraint.constant = 310
        } else if screenHeight == CGFloat(667) {
            topConstraint.constant = 240
        }else if screenHeight == CGFloat(568) {
            topConstraint.constant = 140
        } else if screenHeight == CGFloat(480) {
            topConstraint.constant = 55
        } else {
            topConstraint.constant = 50
        }
    }
    
    @IBAction func getCodeButton(_ sender: UIButton) {
    
        UserDefaults.standard.set("", forKey: "authVerificationID")
        
        var tempStringPhone = phoneTextField.text!.trimmingCharacters(in: .whitespaces)
        tempStringPhone = String(tempStringPhone.filter { !" \n\t\r()-".contains($0) })

        if Validation.validationPhoneNumber(phoneNumber: tempStringPhone) {
            
            self.showActivityIndicator(textLabel: "Проверка телефона")

            let parameters = ["phone": tempStringPhone]
            let request = Network.createRequest(urlString: Network.urlCheckPhone, parameters: parameters).self
            let session = URLSession.shared
            
            session.dataTask(with: request) { (data, response, error) in
                self.hideActivityIndicator()
                
                guard let data = data else { return }
                do {
//                    let json1 = try JSONSerialization.jsonObject(with: data, options: [])
//                    print(json1)
                    let jsonResult = try JSONSerialization.jsonObject(with: data, options:JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                    if jsonResult.value(forKey: "success") as! Bool == false {
                        print("GetCode - success - false")
                        
                        self.showActivityIndicator(textLabel: "Проверка телефона")
                        
                        PhoneAuthProvider.provider().verifyPhoneNumber(tempStringPhone, uiDelegate: nil) { (verificationID, error) in
                            self.hideActivityIndicator()
                            
                            if error == nil {
                                UserDefaults.standard.set(verificationID, forKey: "authVerificationID")
                                self.performSegue(withIdentifier: "SingUp", sender: Any?.self)
                            } else {
                                self.showAlertView(sendingVC: self, titleString: "ERROR", messageString: error?.localizedDescription ?? "")
                                return
                            }
                        }
                    } else {
                        print("GetCode - success - true")
                        self.showAlertView(sendingVC: self, titleString: "WARNING", messageString: "Пользователь уже зарегистрирован. Для продолжения работы пройдите авторизацию.")
                        return
                    }
                } catch {
                    self.showAlertView(sendingVC: self, titleString: "ERROR", messageString: error as! String)
                }
            }.resume()
        } else {
            phoneTextField.text = ""
            phoneTextField.becomeFirstResponder()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "SingUp") {
            let viewController = segue.destination as! GetCodeViewController
            viewController.singUp = true
        }
    }
    
    @IBAction func actionSegment(_ sender: Any) {
        phoneTextField.isHidden = !phoneTextField.isHidden
        mailTextField.isHidden = !mailTextField.isHidden
    }
    
}

extension SingUpViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

        if phoneTextField.text!.count >= 17 && string != "" {
            actionButton.isEnabled = true
            actionButton.backgroundColor = UIColor(red: 0.01, green: 0.30, blue: 0.86, alpha: 1)
        } else {
            actionButton.isEnabled = false
            actionButton.backgroundColor = UIColor.lightGray
        }
        
        return true
    }
    
}
