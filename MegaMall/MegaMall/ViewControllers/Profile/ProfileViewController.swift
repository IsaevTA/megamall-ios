//
//  ProfileViewController.swift
//  MegaMall
//
//  Created by Timur Isaev on 29.11.2019.
//  Copyright © 2019 Timur Isaev. All rights reserved.
//

import UIKit
import StoreKit

class ProfileViewController: UIViewController {
    
    @IBOutlet weak var salerByerSegment: UISegmentedControl!
    @IBOutlet weak var myStoreButton: BorderButton!
    @IBOutlet weak var openStoreButton: BorderButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        SettingsSegment()
    }
    
    func SettingsSegment() {
        switch salerByerSegment.selectedSegmentIndex
        {
        case 0:
            myStoreButton.isHidden = false
            openStoreButton.isHidden = true
        case 1:
            myStoreButton.isHidden = true
            openStoreButton.isHidden = false
        default:
            break
        }
    }
    
    @IBAction func logOffButton(_ sender: Any) {
        
        UserDefaults.standard.set(nil, forKey: "MEGAMALLPHONE")
        UserDefaults.standard.set(nil, forKey: "MEGAMALLUID")
        
        self.goRootViewController()
    }

    @IBAction func rateAppButton(_ sender: Any) {
        if #available(iOS 10, *) {
            SKStoreReviewController.requestReview()

        }
        else if let url = URL(string: "itms-apps://itunes.apple.com/app/1489612567") {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)        }
    }
    
    @IBAction func actionSegment(_ sender: Any) {
        SettingsSegment()        
    }
}
