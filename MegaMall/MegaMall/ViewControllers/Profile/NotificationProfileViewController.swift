//
//  NotificationProfileViewController.swift
//  MegaMall
//
//  Created by Timur Isaev on 07.12.2019.
//  Copyright © 2019 Timur Isaev. All rights reserved.
//

import UIKit

class NotificationProfileViewController: UIViewController {

    @IBOutlet weak var switchOne: UISwitch!
    @IBOutlet weak var switchTwo: UISwitch!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func backButton(_ sender: Any) {
        if((self.presentingViewController) != nil) {
            self.dismiss(animated: true, completion: nil)
        }
    }
}
