//
//  MyProfileViewController.swift
//  MegaMall
//
//  Created by Timur Isaev on 09.12.2019.
//  Copyright © 2019 Timur Isaev. All rights reserved.
//

import UIKit

class MyProfileViewController: UIViewController {

    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var locationTextField: UITextField!
    @IBOutlet weak var profileImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        settingsUI()
    }
    
    func settingsUI() {
        nameTextField.text = currentUser?.name
        phoneTextField.text = currentUser?.phone
        emailTextField.text = currentUser?.email
        locationTextField.text = currentUser?.city
    }
    
    @IBAction func backButton(_ sender: Any) {
        if((self.presentingViewController) != nil) {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func getPhotoButton(_ sender: Any) {
        MBPhotoPicker.shared.showActionSheet(vc: self)
        MBPhotoPicker.shared.imagePickedBlock = { (image) in
            // Here you get your image
            print(image)
            self.profileImage.image = image
            self.sendImageToServer(image: image)
        }
    }
    
    private func sendImageToServer(image: UIImage) {
        NetworkService.uploadAvatar(userID: (currentUser?.id)!, avatarUser: image)
    }
    
    @IBAction func saveButton(_ sender: Any) {
        guard let id = currentUser?.id else { return }
        guard let uid = currentUser?.uid else { return }
        let phone = phoneTextField.text?.trimmingCharacters(in: .whitespaces)
        let token = UserDefaults.standard.string(forKey: "CURRENTTOKENUSER")
        let name = nameTextField.text?.trimmingCharacters(in: .whitespaces)
        let email = emailTextField.text?.trimmingCharacters(in: .whitespaces)
        let location = locationTextField.text?.trimmingCharacters(in: .whitespaces)
        let parameters = [
            "uid": uid,
            "name": name,
            "phone": phone,
            "token": token,
            "email": email,
            "city": location
        ]

        let url: String! = Network.urlEditUser + String(describing: id)

        self.showActivityIndicator(textLabel: "Сохранение данных")

        let request = Network.createRequest(urlString: url, parameters: parameters).self
        let session = URLSession.shared

        session.dataTask(with: request) { (data, response, error) in
            self.hideActivityIndicator()

            guard let data = data else { return }

            do {
//                let json1 = try JSONSerialization.jsonObject(with: data, options: [])
//                print(json1)

                let jsonResult = try JSONSerialization.jsonObject(with: data, options:JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                if jsonResult.value(forKey: "success") as! Bool == false {
                    self.showAlertView(sendingVC: self, titleString: "ERROR", messageString: "Пользователь уже зарегистрирован. Для продолжения работы пройдите авторизацию.")
                } else {
                    self.showAlertView(sendingVC: self, titleString: "GOOD", messageString: "Успешное сохранение.")
                    NetworkService.getUser(vc: self, goToMainViewController: false)
                }

            } catch {
                self.showAlertView(sendingVC: self, titleString: "ERROR", messageString: error as! String)
            }
        }.resume()
        

    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
}
