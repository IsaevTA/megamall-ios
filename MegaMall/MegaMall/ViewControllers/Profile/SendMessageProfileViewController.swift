//
//  SendMessageProfileViewController.swift
//  MegaMall
//
//  Created by Timur Isaev on 07.12.2019.
//  Copyright © 2019 Timur Isaev. All rights reserved.
//

import UIKit
import UITextView_Placeholder

class SendMessageProfileViewController: UIViewController {

    @IBOutlet weak var themeTextField: UITextField!
    @IBOutlet weak var messageTextView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        messageTextView.placeholder = "Текст обращения"
//        messageTextView.placeholderColor = UIColor.lightGray
    }
    
    @IBAction func backButton(_ sender: Any) {
        if((self.presentingViewController) != nil) {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func sendMessageButton(_ sender: Any) {
        
        if((self.presentingViewController) != nil) {
            self.dismiss(animated: true, completion: nil)
        }
    }
}
