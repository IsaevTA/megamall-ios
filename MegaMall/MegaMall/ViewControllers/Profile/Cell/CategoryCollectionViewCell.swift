//
//  CategoryCollectionViewCell.swift
//  MegaMall
//
//  Created by Timur Isaev on 10.12.2019.
//  Copyright © 2019 Timur Isaev. All rights reserved.
//

import UIKit

class CategoryCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var categoryImage: UIImageView!
    @IBOutlet weak var categoryNameLabel: UILabel!
    
}
