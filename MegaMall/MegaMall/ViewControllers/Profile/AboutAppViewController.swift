//
//  aboutAppViewController.swift
//  MegaMall
//
//  Created by Timur Isaev on 07.12.2019.
//  Copyright © 2019 Timur Isaev. All rights reserved.
//

import UIKit

class AboutAppViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func backButton(_ sender: Any) {
        if((self.presentingViewController) != nil) {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
}
