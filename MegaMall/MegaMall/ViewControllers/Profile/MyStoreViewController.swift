//
//  MyStoreViewController.swift
//  MegaMall
//
//  Created by Timur Isaev on 09.12.2019.
//  Copyright © 2019 Timur Isaev. All rights reserved.
//

import UIKit

class MyStoreViewController: UIViewController {

        var arrText = [MyStore](
            arrayLiteral: MyStore(name: "Союз-строй", type: "Транспорт", count: 2, urlImage: ""),
            MyStore(name: "House", type: "Недвижемость", count: 1, urlImage: ""),
            MyStore(name: "Автохелп", type: "Транспорт", count: 0, urlImage: ""),
            MyStore(name: "Плюшка-хрюшка", type: "Транспорт", count: 0, urlImage: ""),
            MyStore(name: "Лама", type: "Для дома и дачи", count: 1, urlImage: ""),
            MyStore(name: "Том-строер", type: "Для дома и дачи", count: 0, urlImage: ""),
            MyStore(name: "Avto", type: "Транспорт", count: 0, urlImage: ""),
            MyStore(name: "Автохелп", type: "Транспорт", count: 0, urlImage: ""),
            MyStore(name: "Плюшка-хрюшка", type: "Транспорт", count: 0, urlImage: ""),
            MyStore(name: "Лама", type: "Для дома и дачи", count: 1, urlImage: ""),
            MyStore(name: "Том-строер", type: "Для дома и дачи", count: 0, urlImage: "")
        )
        
        override func viewDidLoad() {
            super.viewDidLoad()
        }
        
        @IBAction func backButton(_ sender: Any) {
            if((self.presentingViewController) != nil) {
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    // MARK: - Table view data source
    extension MyStoreViewController: UITableViewDelegate, UITableViewDataSource {
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return arrText.count
        }

        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell: CategoryTableViewCell = tableView.dequeueReusableCell(withIdentifier: "categoryCell", for: indexPath) as! CategoryTableViewCell
            
            cell.nameLabel.text = arrText[indexPath.row].name
            cell.typeLabel.text = arrText[indexPath.row].type
            cell.countLabel.text = String(arrText[indexPath.row].count) + " товаров"
            if arrText[indexPath.row].urlImage != "" {
                cell.categoryImage.image = UIImage(named: arrText[indexPath.row].urlImage)
            }

            return cell
        }
        
        
    }

