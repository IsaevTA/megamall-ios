//
//  CategoryStoreViewController.swift
//  MegaMall
//
//  Created by Timur Isaev on 08.12.2019.
//  Copyright © 2019 Timur Isaev. All rights reserved.
//

import UIKit

class CategoryStoreViewController: UIViewController {

    private let itemsPerRow: CGFloat = 3
    //private let sectionInsets = UIEdgeInsets(top: 50, left: 10, bottom: 50, right: 10)
    let sectionInsets = UIEdgeInsets(top: 16.0, left: 16.0, bottom: 16.0, right: 16.0)
    private let minimumItemSpacing: CGFloat = 8
    
    var arrText = [CategoryStore](
        arrayLiteral: CategoryStore(name: "Транспорт", urlImage: ""),
        CategoryStore(name: "Недвижемость", urlImage: ""),
        CategoryStore(name: "Одежда, обувь и акскссуары", urlImage: ""),
        CategoryStore(name: "Товары для детей и игрушки", urlImage: ""),
        CategoryStore(name: "Красота и здоровье", urlImage: ""),
        CategoryStore(name: "Для дома и дачи", urlImage: ""),
        CategoryStore(name: "Продукты притания", urlImage: ""),
        CategoryStore(name: "Электроника", urlImage: ""),
        CategoryStore(name: "Интересы", urlImage: ""),
        CategoryStore(name: "18+", urlImage: ""),
        CategoryStore(name: "17+", urlImage: ""),
        CategoryStore(name: "16+", urlImage: ""),
        CategoryStore(name: "15+", urlImage: ""),
        CategoryStore(name: "14+", urlImage: ""),
        CategoryStore(name: "13+", urlImage: ""),
        CategoryStore(name: "12+", urlImage: ""),
        CategoryStore(name: "11+", urlImage: ""),
        CategoryStore(name: "10+", urlImage: "")
    )

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func backButton(_ sender: Any) {
        if((self.presentingViewController) != nil) {
            self.dismiss(animated: true, completion: nil)
        }
    }
}
// MARK: - UICollectionViewDataSource
extension CategoryStoreViewController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrText.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: CategoryCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "categoryCell", for: indexPath) as! CategoryCollectionViewCell
        cell.categoryNameLabel.text = arrText[indexPath.row].name
        if arrText[indexPath.row].urlImage != "" {
            cell.categoryImage.image = UIImage(named: arrText[indexPath.row].urlImage)
        }
        //cell.backgroundColor = .gray
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let paddingSpace = sectionInsets.left + sectionInsets.right + minimumItemSpacing * (itemsPerRow - 1)
        let availableWidth = collectionView.bounds.width - paddingSpace
        let widthPerItem = availableWidth / itemsPerRow
        return CGSize(width: widthPerItem, height: widthPerItem)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return minimumItemSpacing
    }
    

}
