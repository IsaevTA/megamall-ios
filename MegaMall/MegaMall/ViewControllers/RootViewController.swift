//
//  ViewController.swift
//  MegaMall
//
//  Created by Timur Isaev on 20.11.2019.
//  Copyright © 2019 Timur Isaev. All rights reserved.
//

import UIKit

class RootViewController: UIViewController {


    @IBOutlet weak var logoView: UIView!
    @IBOutlet weak var buttonDownStackView: UIStackView!
    
    let phoneCurrentUser = UserDefaults.standard.string(forKey: "MEGAMALLPHONE")
    let uidCurrentUser = UserDefaults.standard.string(forKey: "MEGAMALLUID")

    override func viewDidLoad() {
        super.viewDidLoad()
        
        login()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        UISettings()
    }
    
    func UISettings() {
        navigationController?.isNavigationBarHidden = true
    }
    
    @IBAction func loginButton(_ sender: UIButton) {
        login()
    }
    
    func login() {
        if phoneCurrentUser != nil && uidCurrentUser != nil {
            //let parameters = ["phone": phoneCurrentUser, "uid": uidCurrentUser]
            
            NetworkService.getUser(vc: self, goToMainViewController: true)
            
        }
    }
    @IBAction func singUpButton(_ sender: UIButton) {

    }

}

