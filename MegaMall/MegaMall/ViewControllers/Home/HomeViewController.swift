//
//  HomeViewController.swift
//  MegaMall
//
//  Created by Timur Isaev on 23.11.2019.
//  Copyright © 2019 Timur Isaev. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import GoogleMaps

private enum State {
    case closed
    case half
    case open
}

extension State {
    var opposite: State {
        switch self {
        case .open: return .closed
        case .half: return .half
        case .closed: return .open
        }
    }
}

class HomeViewController: UIViewController, GMSMapViewDelegate {

    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var showListViewButton: UIButton!
    @IBOutlet weak var listView: UIView!
    @IBOutlet weak var listViewHightConstraint: NSLayoutConstraint!
    @IBOutlet weak var listViewBottomConstraint: NSLayoutConstraint!

    let locationManager = CLLocationManager()
    var zoom : Float = 14

    var statusBarFrameHeight: CGFloat = 0
    var navigationBarHeight: CGFloat = 0
    var tapBarFrameHeight: CGFloat = 0
    var listViewHeight: CGFloat = 0

    private var currentState: State = .closed
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationManager.requestAlwaysAuthorization()
        locationManager.requestWhenInUseAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
        
        //Настройка mapView
        mapView.delegate = self
        mapView.setMinZoom(4, maxZoom: 26)
        mapView.clear()
        mapView.isMyLocationEnabled = true
        mapView.settings.myLocationButton = true

        statusBarFrameHeight = UIApplication.shared.statusBarFrame.height
        navigationBarHeight = navigationController?.navigationBar.frame.size.height ?? 0
        tapBarFrameHeight = tabBarController?.tabBar.frame.size.height ?? 0
        listViewHeight = UIScreen.main.bounds.height - statusBarFrameHeight - navigationBarHeight - tapBarFrameHeight - 20
            
        showListViewButton.layer.cornerRadius = showListViewButton.bounds.height / 2
        listView.layer.cornerRadius = 10
        listViewHightConstraint.constant = listViewHeight
        listViewBottomConstraint.constant = listViewHeight

        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(didSwipeOnView(_:)))
        listView.addGestureRecognizer(panGesture)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        for object in mapView.subviews{
            print(object.description)
            if object.description.contains("GMSUISettingsPaddingView") {
                for obj in object.subviews {
                    print(obj.description)
                    if obj.description.contains("GMSUISettingsView") {
                        for view in obj.subviews {
                            print(view.description)
                            if view.description.contains("GMSx_QTMButton") {
                                var frame = view.frame;
                                frame.origin.y = statusBarFrameHeight + navigationBarHeight + 24;
                                frame.origin.x = 16
                                view.frame = frame;
                            }
                        }
                    }
                }
            }
        }
    }
    
    @objc func didSwipeOnView(_ sender: UIPanGestureRecognizer) {
        if sender.isEnabled {
            if case .Down = sender.verticalDirection(target: listView) {
                switch currentState {
                case .half:
                    listViewBottomConstraint.constant = -tapBarFrameHeight - listViewHeight
                    currentState = .closed
                case .open:
                    listViewBottomConstraint.constant = -tapBarFrameHeight - listViewHeight * 0.7
                    currentState = .half
                default: break
                    
                }
            } else {
                if currentState == .half {
                    listViewBottomConstraint.constant = -tapBarFrameHeight
                    currentState = .open
                }
            }
        }
    }
/*
    var distanceCounter: double = 0
    @objc func xButtonWasDragged(_ sender: UIPanGestureRecognizer) {
        if sender.state == UIGestureRecognizer.State.began || sender.state == UIGestureRecognizer.State.changed {
            self.view.bringSubviewToFront(listView)
            let translation = sender.translation(in: self.view)
            //Move the view
            listView.center = CGPoint(x: listView.center.x + translation.x, y: listView.center.y)
            distanceCounter += translation.y
            //if moved left dont add to our distance counter
            if distanceCounter < 0 {
                distanceCounter = 0
            }
            // prevent button from moving left from start
            if xButton.center.x < startingPointxBtn {
                xButton.center.x = startingPointxBtn
            } else if xButton.center.x > largeCircle.center.x && distanceCounter > xBtnDistance {
                xButton.center.x = largeCircle.center.x
                distanceCounter = xBtnDistance
                // If we hit our target center and go over it in our distance counter
                // make our distance be reached and forced to the center
            } else {
                // set the translation
                sender.setTranslation(CGPoint.zero, in: self.view)
            }
        } else if sender.state == UIGestureRecognizerState.ended {
            // If you let go of button within a distance, snap to center
            if largeCircle.center.x - xButton.center.x <= 35.0 {
                xButton.center = largeCircle.center
            } else {
                // if none of these conditions meet make it snap to start
                xButton.center.x = startingPointxBtn
            }
            // reset our counter to always begin with zero
            distanceCounter = 0
        }
    }
*/
    func mapView(_ mapView: GMSMapView, idleAt cameraPosition: GMSCameraPosition) {

    }
    
    @IBAction func zoomPlusButtonTapped(_ sender: UIButton) {

        if zoom < mapView.maxZoom {

            zoom = zoom + 1
            self.mapView.animate(toZoom: zoom)
        }
    }

    @IBAction func zoomMinusButtonTapped(_ sender: UIButton) {

        if zoom > mapView.minZoom {

            zoom = zoom - 1
            self.mapView.animate(toZoom: zoom)
        }
    }
    
    @IBAction func showListViewButtonTapped(_ sender: Any) {
 
        listViewBottomConstraint.constant = -tapBarFrameHeight - listViewHeight * 0.7
        currentState = .half
    }
}


extension HomeViewController: CLLocationManagerDelegate {

    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {

        switch status {
        case .notDetermined:
            locationManager.requestWhenInUseAuthorization()
        default:
            locationManager.startUpdatingLocation()
        }
        if status == .authorizedWhenInUse {

        }
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {

        if let location = locations.first {

            mapView.camera = GMSCameraPosition(target: location.coordinate, zoom: zoom, bearing: 0, viewingAngle: 0)

            // определяем адрес
            //reverseGeocodeCoordinate(coordinate: location.coordinate)

            locationManager.stopUpdatingLocation()
        }
        
    }
}

extension UIPanGestureRecognizer {

    enum GestureDirection {
        case Up
        case Down
        case Left
        case Right
    }

    /// Get current vertical direction
    ///
    /// - Parameter target: view target
    /// - Returns: current direction
    func verticalDirection(target: UIView) -> GestureDirection {
        return self.velocity(in: target).y > 0 ? .Down : .Up
    }

    /// Get current horizontal direction
    ///
    /// - Parameter target: view target
    /// - Returns: current direction
    func horizontalDirection(target: UIView) -> GestureDirection {
        return self.velocity(in: target).x > 0 ? .Right : .Left
    }

    /// Get a tuple for current horizontal/vertical direction
    ///
    /// - Parameter target: view target
    /// - Returns: current direction
    func versus(target: UIView) -> (horizontal: GestureDirection, vertical: GestureDirection) {
        return (self.horizontalDirection(target: target), self.verticalDirection(target: target))
    }

}
