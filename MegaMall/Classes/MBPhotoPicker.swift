//
//  MBPhotoPicker.swift
//  MegaMall
//
//  Created by Timur Isaev on 09.12.2019.
//  Copyright © 2019 Timur Isaev. All rights reserved.
//

import UIKit
import AVFoundation
import Photos

let photoAccessMessage = "Seems like you denied access to Library. Go to Settings and allow access."
let cameraAccessMessage = "Seems like you denied access to Camera. Go to Settings and allow access."

class MBPhotoPicker: NSObject {

    static let shared = MBPhotoPicker()
    fileprivate var currentVC: UIViewController!
    var imagePickedBlock: ((UIImage) -> Void)?
    
    func showActionSheet(vc: UIViewController) {
        currentVC = vc
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Photos", style: .default, handler: { (alert:UIAlertAction!) -> Void in
            self.checkForLibraryPermission()
        }))
        actionSheet.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (alert:UIAlertAction!) -> Void in
            self.checkForCameraPermission()
        }))
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        vc.present(actionSheet, animated: true, completion: nil)
    }
    
    func checkForCameraPermission() {
        
        let cameraMediaType = AVMediaType.video
        let cameraAuthorizationStatus = AVCaptureDevice.authorizationStatus(for: cameraMediaType)
        switch cameraAuthorizationStatus {
            
        case .authorized:
            // Access is granted by user.
            DispatchQueue.main.async {
                self.userCamera()
            }
            break
        case .notDetermined:
            // It is not determined until now.
            AVCaptureDevice.requestAccess(for: cameraMediaType) { (status) in
                if status == true {
                    DispatchQueue.main.async {
                        self.userCamera()
                    }
                }
                else {
                    // Access has been denied.
                }
            }
            break
        case .restricted:
            // User do not have access to camera.
            self.askForPermissionWithTitle(title: cameraAccessMessage)
            break
        case .denied:
            // User has denied the permission.
            self.askForPermissionWithTitle(title: cameraAccessMessage)
            break
        @unknown default:
            return
        }
    }
    
    func checkForLibraryPermission() {
        
        let photoAuthorizationStatus = PHPhotoLibrary.authorizationStatus()
        switch photoAuthorizationStatus {
            
        case .authorized:
            // Access is granted by user.
            DispatchQueue.main.async {
                self.userLibrary()
            }
            break
        case .notDetermined:
            // It is not determined until now.
            PHPhotoLibrary.requestAuthorization { (status) in
                if status == PHAuthorizationStatus.authorized {
                    DispatchQueue.main.async {
                        self.userLibrary()
                    }
                }
                else {
                    // Access has been denied.
                }
            }
            break
        case .restricted:
            // User do not have access to photo album.
            self.askForPermissionWithTitle(title: photoAccessMessage)
            break
            
        case .denied:
            // User has denied the permission.
            self.askForPermissionWithTitle(title: photoAccessMessage)
            break
        @unknown default:
            return
        }
    }
    
    func userCamera() {
        let imagePicker = UIImagePickerController()
        imagePicker.sourceType = .camera
        imagePicker.allowsEditing = true
        imagePicker.delegate = self as UIImagePickerControllerDelegate & UINavigationControllerDelegate
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
            imagePicker.modalPresentationStyle = .popover
            imagePicker.popoverPresentationController?.delegate = self as? UIPopoverPresentationControllerDelegate
            imagePicker.popoverPresentationController?.sourceView = currentVC.view
        }
        currentVC.present(imagePicker, animated: true, completion: nil)
    }
    
    func userLibrary() {
        let imagePicker = UIImagePickerController()
        imagePicker.sourceType = .photoLibrary
        imagePicker.allowsEditing = true
        imagePicker.delegate = self as UIImagePickerControllerDelegate & UINavigationControllerDelegate
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
            imagePicker.modalPresentationStyle = .popover
            imagePicker.popoverPresentationController?.delegate = self as? UIPopoverPresentationControllerDelegate
            imagePicker.popoverPresentationController?.sourceView = currentVC.view
        }
        currentVC.present(imagePicker, animated: true, completion: nil)
    }
    
    func askForPermissionWithTitle(title:String) {
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (_) in
            
        }
        let settingsAction = UIAlertAction(title: "Settings", style: .default) { (_) in
            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else { return }
            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl)
            }
        }
        
        let actionSheet = UIAlertController(title: "Access Denied", message: title, preferredStyle: .alert)
        actionSheet.addAction(cancelAction)
        actionSheet.addAction(settingsAction)
        currentVC.present(actionSheet, animated: true) {
            
        }
    }
}

extension MBPhotoPicker: UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        currentVC.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            self.imagePickedBlock?(image)
        }
        currentVC.dismiss(animated: true, completion: nil)
    }

}
