//
//  Category.swift
//  MegaMall
//
//  Created by Timur Isaev on 08.12.2019.
//  Copyright © 2019 Timur Isaev. All rights reserved.
//

import Foundation

struct MyStore: Codable {
    var name: String
    var type: String
    var count: Int
    var urlImage: String
}
