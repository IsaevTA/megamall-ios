//
//  CategoryStore.swift
//  MegaMall
//
//  Created by Timur Isaev on 10.12.2019.
//  Copyright © 2019 Timur Isaev. All rights reserved.
//

import Foundation

struct CategoryStore: Codable {
    var name: String
    var urlImage: String
}
