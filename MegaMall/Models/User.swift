//
//  User.swift
//  MegaMall
//
//  Created by Timur Isaev on 04.12.2019.
//  Copyright © 2019 Timur Isaev. All rights reserved.
//

import Foundation

struct User: Codable {
    let success: Bool?
    let error: Bool?
    let user: UserDetail
}

struct UserDetail: Codable {
    let address: String?
    let api_token: String?
    let birthday: String?
    let city: String?
    let created_at: String?
    let description: String?
    let email: String?
    let email_verified_at: String?
    let id: Int?
    let latitude: String?
    let longitude: String?
    let name: String?
    let phone: String?
    let photo: String?
    let privileges: Int?
    let sex: String?
    let token: String?
    let type: String?
    let uid: String?
    let updated_at: String?
}
