//
//  Button.swift
//  MegaMall
//
//  Created by Timur Isaev on 04.12.2019.
//  Copyright © 2019 Timur Isaev. All rights reserved.
//

import Foundation
import UIKit

class BorderButton: UIButton {
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        initial()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initial()
    }
    
    func initial() {
        clipsToBounds = true
        layer.cornerRadius = 5
        layer.borderWidth = 1
        layer.borderColor = UIColor.gray.cgColor
    }
}
