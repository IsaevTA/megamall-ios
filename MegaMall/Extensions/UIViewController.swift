//
//  UIViewController.swift
//  MegaMall
//
//  Created by Timur Isaev on 26.11.2019.
//  Copyright © 2019 Timur Isaev. All rights reserved.
//

import Foundation
import UIKit

var activityIndicator = UIAlertController()

// activityIndicator
var container: UIView = UIView()
var label: UILabel = UILabel()
var loadingView: UIView = UIView()
let activityIndicatorNew: UIActivityIndicatorView = UIActivityIndicatorView()

extension UIViewController {

    private func UIColorFromHex(rgbValue:UInt32, alpha:Double=1.0)->UIColor {
        let red = CGFloat((rgbValue & 0xFF0000) >> 16)/256.0
        let green = CGFloat((rgbValue & 0xFF00) >> 8)/256.0
        let blue = CGFloat(rgbValue & 0xFF)/256.0
        
        return UIColor(red:red, green:green, blue:blue, alpha:CGFloat(alpha))
    }
    
    func showActivityIndicator(textLabel: String) {

        DispatchQueue.main.async {
            container.frame = self.view.frame
            container.center = self.view.center
            container.backgroundColor = self.UIColorFromHex(rgbValue: 0xffffff, alpha: 0.3)
            
            loadingView.frame = CGRect(x: 0, y: 0, width: 80, height: 80)
            loadingView.center = self.view.center
            loadingView.backgroundColor = self.UIColorFromHex(rgbValue: 0x444444, alpha: 0.7)
            loadingView.clipsToBounds = true
            loadingView.layer.cornerRadius = 10
            
            activityIndicatorNew.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
            if #available(iOS 13.0, *) {
                activityIndicatorNew.style = UIActivityIndicatorView.Style.large
            }
            activityIndicatorNew.center = CGPoint(x: loadingView.frame.size.width / 2, y: loadingView.frame.size.height / 2)
            
//            label.frame = CGRect(x: 0, y: 0, width: 80, height: 80)
//            label.backgroundColor = self.UIColorFromHex(rgbValue: 0xffffff, alpha: 0.3)
//            label.textColor = UIColor(red: 0.706, green: 0.737, blue: 0.784, alpha: 1)
//            label.font = UIFont(name: "SFProDisplay-Bold", size: 12)
//            label.textAlignment = .center
//            label.attributedText = NSMutableAttributedString(string: textLabel, attributes: [NSAttributedString.Key.kern: -0.29])
//
//
//            container.addSubview(label)
            loadingView.addSubview(activityIndicatorNew)
            container.addSubview(loadingView)
            self.view.addSubview(container)
            
            activityIndicatorNew.startAnimating()
        }
        
//        DispatchQueue.main.async {
//            activityIndicator = UIAlertController(title: nil, message: textLabel + "...", preferredStyle: .alert)
//
//            let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
//            loadingIndicator.hidesWhenStopped = true
//            loadingIndicator.style = UIActivityIndicatorView.Style.medium
//            loadingIndicator.startAnimating();
//
//            activityIndicator.view.addSubview(loadingIndicator)
//            self.present(activityIndicator, animated: true, completion: nil)
//        }
    }

    func hideActivityIndicator() {
        DispatchQueue.main.async {
            //activityIndicator.dismiss(animated: true, completion: nil)
            activityIndicatorNew.stopAnimating()
            container.removeFromSuperview()
        }
    }

    func showAlertView(sendingVC: UIViewController, titleString : String , messageString: String) ->() {

        DispatchQueue.main.async {
            let alertView = UIAlertController(title: titleString, message: messageString, preferredStyle: .alert)

            let alertAction = UIAlertAction(title: "OK", style: .cancel) { (alert) in
                self.dismiss(animated: true, completion: nil)
            }
            alertView.addAction(alertAction)
            sendingVC.present(alertView, animated: true, completion: nil)
//            self.dismiss(animated: true) {
//                //self.present(alertView, animated: true)
//                self.present(alertView, animated: true, completion: nil)
//            }
        }
    }
    
//    func goStoryboard(storyboard: String, viewController: UIViewController) {
//        DispatchQueue.main.async {
//            let goingStoryboard = self.storyboard?.instantiateViewController(identifier: storyboard) as? viewController
//            self.view.window?.rootViewController = goingStoryboard
//            self.view.window?.makeKeyAndVisible()
//        }
//    }
    
//    func goMainViewController() {
//        DispatchQueue.main.async {
//
////            if let vc = UIStoryboard(name: "Home", bundle: nil).instantiateInitialViewController() {
////                vc.modalTransitionStyle = .crossDissolve
////                vc.modalPresentationStyle = .overCurrentContext
////                self.present(vc, animated: true, completion: nil)
////            }
//
//            let goingStoryboard = self.storyboard?.instantiateViewController(identifier: Constants.Storyboard.navigationController) as? TabBarViewController
//            self.view.window?.rootViewController = goingStoryboard
//            self.view.window?.makeKeyAndVisible()
//        }
//    }
    
//    func goRootViewController() {
//        DispatchQueue.main.async {
//            let goingStoryboard = self.storyboard?.instantiateViewController(identifier: Constants.Storyboard.rootViewController) as? RootViewController
//            self.view.window?.rootViewController = goingStoryboard
//            self.view.window?.makeKeyAndVisible()
//        }
//    }
    
    func goMainViewController() {
        DispatchQueue.main.async {
            let storyboard = UIStoryboard(name: "Home", bundle: nil)
            if #available(iOS 13.0, *) {
                if let viewController = storyboard.instantiateViewController(identifier: Constants.Storyboard.tabBarViewController) as? TabBarViewController {
                    self.view.window?.rootViewController = viewController
                }
            } else {
                if let viewController = storyboard.instantiateViewController(withIdentifier: Constants.Storyboard.tabBarViewController) as? TabBarViewController {
                    self.view.window?.rootViewController = viewController
                }
            }
            self.view.window?.makeKeyAndVisible()
        }
    }
    
    func goRootViewController() {
        DispatchQueue.main.async {
            let storyboard = UIStoryboard(name: "AuthRegUser", bundle: nil)
            if #available(iOS 13.0, *) {
                if let viewController = storyboard.instantiateViewController(identifier: Constants.Storyboard.navigationRootViewController) as? NavigationRootViewController {
                    self.view.window?.rootViewController = viewController
                }
            } else {
                if let viewController = storyboard.instantiateViewController(withIdentifier: Constants.Storyboard.navigationRootViewController) as? NavigationRootViewController {
                    self.view.window?.rootViewController = viewController
                }
            }
            self.view.window?.makeKeyAndVisible()
        }
    }
}
