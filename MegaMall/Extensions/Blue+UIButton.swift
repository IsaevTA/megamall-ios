//
//  Blue+UIButton.swift
//  MegaMall
//
//  Created by Timur Isaev on 04.12.2019.
//  Copyright © 2019 Timur Isaev. All rights reserved.
//

import Foundation
import UIKit

class BlueButton: UIButton {
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        initial()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initial()
    }
    
    func initial() {
        layer.cornerRadius = 5
        backgroundColor = UIColor(red: 0.01, green: 0.30, blue: 0.86, alpha: 1)
    }
}
