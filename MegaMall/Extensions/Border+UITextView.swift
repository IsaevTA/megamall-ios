//
//  Border+UITextView.swift
//  MegaMall
//
//  Created by Timur Isaev on 07.12.2019.
//  Copyright © 2019 Timur Isaev. All rights reserved.
//

import Foundation
import UIKit

class BorderTextView: UITextView {
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        initial()
    }
    
    func initial() {
        layer.borderColor = UIColor.lightGray.cgColor
        layer.borderWidth = 1.0;
        layer.cornerRadius = 8;
    }
}
